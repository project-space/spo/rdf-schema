# RDF Schema


## SPO 2024.1 Dataset

This version of the schema used for the SPO NDS project is a subset of SPO 2024.1, which extends [SPHN 2024.2](https://www.biomedit.ch/rdf/sphn-schema/sphn/2024/2).
> ⚠️ Since the release, SPHN had made two corrections: the cardinalities of 1) hasOutput of Sample Processing and 2) hasNotation of Variant Descriptor have both changed from 0:1 to 0:n ⚠️


The SPO 2024.1 dataset representation is available in this [.XLSX file](dataset/SPO_dataset_release_2024_1_20240503.xlsx) and follows the same strucure as this of SPHN 2024.2.

Links to SPO 2024.1:
 - [**dataset**](dataset/SPO_dataset_release_2024_1_20240503.xlsx)
 - [**doc**](doc/)
 - [**schema**](schema/)


Links to SPHN 2024.2:
 - [**dataset**](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/blob/aa3e0783703cf03bc6f8748f2bb408412cf1a139/dataset/SPHN_dataset_release_2024_2_20240502.xlsx)
 - [**doc**](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/blob/aa3e0783703cf03bc6f8748f2bb408412cf1a139/documentation/sphn_schema_2024.2.html)
 - [**schema**](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/blob/aa3e0783703cf03bc6f8748f2bb408412cf1a139/rdf_schema/sphn_rdf_schema.ttl)


 [Link to downloads of .TTL files, documentations, SHACL rules and QC/CC SPARQL of the SPHN dataset 2024-2 release](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/releases/2024-2).


## External terminologies

The new external terminologies used by SPO are **NCI Thesaurus**, **OncoTree** and **ICD-O-3**.
[link to the external terminology repo](https://git.dcc.sib.swiss/project-space/spo/external-terminologies/-/releases/spo-schema-2024.1-release-amended-fix-NCIT-ttl).



## Changes since pre-release
### Changes from this amendment
 - new concept spo:Oncology Diagnosis that overwrites Oncology Diagnosis; its code attribute is connected to OncoTree, ICD-O-3 and ICD-10-GM
 - ICD-O-3 external terminology is provided
 - OncoTree external terminology is provided
### Changes from the release prior this amendment
 - NCIT external terminology is provided
 - Biochemical Oncology Disease Assessment Component removed
 - Clinical Oncology Disease Assessment Component removed
 - Radiological Oncology Disease Assessment Component removed
 - Oncology Disease Assessment Dissociation Component removed
 - Oncology Disease Assessment Dissociation Result removed
 - object property spo:hasConfidenceCode removed from Oncology Disease Assessment Result
 - object proerty spo:hasDissociatedEvolution added to Oncology Disease Assessment Result
 - Biochemical Oncology Disease Assessment added
 - Clinical Oncology Disease Assessment added
 - Radiological Oncology Disease Assessment added
 - object property hasMethodCode pointing to LOINC and SNOMED CT tumor markers added for biochemical oncology disease assessment
 - new object property spo:hasCancerTreatmentComplex added to Allogeneic Transplantation (for representation of a conditioning treatment)


## Git clone

The first time you clone the directory, run the following command to also pull the SPHN 2023.2 in sphn-ontology folder:
```shell
git submodule update --init
``` 


