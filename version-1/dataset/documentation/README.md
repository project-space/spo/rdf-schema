# Documentation

You will find here more detailed descriptions of the concept rationale, structures and development history.

- Everything under the [legacy](legacy) subfolder only serve a historical purpose and should not be used to guide the instanciations of the concepts.
- References to TNM in its original version can be found here: [SPHN_dataset_release_2023_2_20230317.xlsx](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/blob/1d2bf5daec3d7f4e41930941fe7bdec9386a3f36/dataset/SPHN_dataset_release_2023_2_20230317.xlsx) .
- spo:Oncology Diagnosis has no documentation because it has the exact same structure as [Oncology Diagnosis](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/blob/aa3e0783703cf03bc6f8748f2bb408412cf1a139/dataset/documentation-2024.1/OncologyDiagnosis_2024.1_final.pdf). This override is needed to connect the code attribute to new external terminologies.
