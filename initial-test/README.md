# RDF Schema

> 🚨 *Empty folders: please read all parts of the README.md* 🚨


## SPHN 2023.2 Dataset

This version of the schema used for the SPO NDS project is a subset of [SPHN 2023.2](https://www.biomedit.ch/rdf/sphn-ontology/sphn/2023/2).
The dataset, doc and schema directories are empty as no new concept is used.

Links to SPHN 2023.2:
 - [**dataset**](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology/-/blob/2023-2/dataset/SPHN_dataset_release_2023_2_20230317.xlsx?ref_type=tags)
 - [**doc**](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology/-/blob/2023-2/documentation/sphn_rdf_schema_documentation.html?ref_type=tags)
 - [**schema**](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology/-/blob/2023-2/rdf_schema/sphn_rdf_schema.ttl?ref_type=tags)


 [Link to downloads of .TTL files, documentations, SHACL rules and QC/CC SPARQL of the SPHN dataset 2023-2 release](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology/-/releases/2023-2).

## Subset used in this version of the DTR

The subset is made of the following concepts:
 - Administrative Case
 - Administrative Gender
 - Birth Date
 - Body Height
 - Body Weight
 - Data Provider Institute
 - Death Date
 - Death Status
 - Drug Administration Event
 - FOPH Diagnosis
 - FOPH Procedure
 - Lab Result
 - Lab Test

## Git clone

The first time you clone the directory, run the following command to also pull the SPHN 2023.3 in sphn-ontology folder:
```shell
git submodule update --init
``` 