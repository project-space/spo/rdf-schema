# Documentation

You will find here more detailed descriptions of the concept rationale, structures and development history.

- Everything under the [legacy](legacy) subfolder only serves a historical purpose and should not be used to guide the instanciations of the concepts.
- References to TNM in its original version can be found here: [SPHN_dataset_release_2023_2_20230317.xlsx](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/blob/1d2bf5daec3d7f4e41930941fe7bdec9386a3f36/dataset/SPHN_dataset_release_2023_2_20230317.xlsx) .

## Change overview from version 1 (SPO 2024.1) to version 2 (SPO 2025.1):

### Change requests

- **Oncology Diagnosis:** Oncology Diagnosis from SPHN has now the same structure than spo:Oncology Diagnosis, then the latter has been removed from SPO Dataset
- **Pathology Tumor Assessment, Pathology Tumor Assessment Component:** Add “code restricted to: descendant of: 1222601005 |American Joint Committee on Cancer residual tumor allowable value (qualifier value)|” to the result of the assessement itself. The staging code RX, R0, R1 and the measurement value were put in the Component. This design is however too complicated and it would be easier to put the stage directly in the result attribute of Pathology Tumor Assessment. The measurement itself is not expected to be used in our project, but in case it is provided, another instance of assessment can be implemented.
- **Cancer Treatment Complex, Systemic Cancer Therapy, Cancer Therapy Regimen, TNM Classification:** Change cardinality of Source System form 1:1 to 1:n . With this new cardinality, these concepts will conform to all other concepts, as they have cardinality 1:n for Source System.
- **Biochemical Oncology Disease Assessment:** Relax restriction on SNOMED CT value set of the methode code, include descendant of 'Measurement procedure': 122869004 | Measurement procedure (procedure). There was need to allow other type of biochemical measurements  to evaluate disease progression (like 88308000 | Blood cell count (procedure) |).
- **Oncology Disease Assessment Result:** Change the contextualized description of string  value result attribute to “textual representation specifying the disease evolution, for example the conclusion of a radiology report“. This indicates to the user (data engineer or data analyst) that this field can be used for the conclusion of the radiology report.
- **Allogeneic Transplantation:** Replace “CHOP or SNOMED CT“ in Standard column by “CHOP; SNOMED CT“. This is a syntax error, and causes trouble in SPHN tools.
- **Molecular Test:** Open Biomarker feature to NCIT, LOINC and SNOMED CT, remove the restriction on the Value Set. Before the code atttribute was used for LOINC and SNOMED CT and biomarker feature code was to use only for NCIT codes. Now, only the attribute biomarker feature code will be used for sake of simplicity for the data providers, and we need to extends the possible standards to LOINC and SNOMED CT and remove the too restrictive value set.
- **Oncology Disease Assessment, Biochemical Oncology Disease Assessment, Clinical Oncology Disease Assessment, Radiological Oncology Disease Assessment:** Remove the constraint on code attribute. This constraint followed the parent concept Assessment in SPHN 2024.2. In SPHN 2025.1, the standards and the value set of this element have opened, and there is no reason to keep the previous restrictions on inheriting concepts

### New omics concepts

- **Research Instrument**
- **Pathology Image Digitization**
- **Antitumor Drug Screening Assay**
- **Drug Panel**
- **Antitumor Drug Screening Condition**
- **Antibody**
- **Marker Panel**
