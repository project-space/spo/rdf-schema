# RDF Schema


## SPO 2025.1 Dataset

This version of the schema used for the SPO NDS project extends [SPHN 2025.1](https://www.biomedit.ch/rdf/sphn-schema/sphn/2025/1) and is intended to deprecate [SPO Schema 2024.1](https://www.biomedit.ch/rdf/sphn-schema/spo/2024/1).


The SPO 2024.1 dataset representation is available in this [.XLSX file](dataset/SPO_dataset_release_2024_1_20240503.xlsx) and follows the same strucure as this of SPHN 2024.2.

Links to SPO 2024.1:
 - [**dataset**](dataset/SPO_dataset_release_2024_1_20240503.xlsx)
 - [**doc**](doc/)
 - [**schema**](schema/)


Links to SPHN 2025.1:
 - [**dataset**](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/blob/46a3e3c6738820fcd9aac06d8bd659368ab1dbaf/dataset/SPHN_dataset_release_2025_1_20250115.xlsx)
 - [**doc**](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/blob/46a3e3c6738820fcd9aac06d8bd659368ab1dbaf/documentation/sphn_schema_2025.1.html)
 - [**schema**](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/blob/46a3e3c6738820fcd9aac06d8bd659368ab1dbaf/rdf_schema/sphn_rdf_schema.ttl)


 [Link to downloads of .TTL files, documentations, SHACL rules and QC/CC SPARQL of the SPHN dataset 2024-2 release](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/releases/2024-2).

## Changes in SPO concepts from SPO Schema 2024.1
 - **Pathology Tumor Assessment**: the Component is not to be used anymore; the residual tumor staging (RX, R0, R1) is to put in the result of the Pathology Tumor Assessment
 - **Pathology Tumor Assessment Component**: removed
 - **Oncology Diagnosis**: the SPO spo:Oncology Diagnosisis  removed in favor of the SPHN Oncology Diagnosis
 - **Cancer Treatment Complex, Systemic Cancer Therapy, Cancer Therapy Regimen, TNM Classification**: cardinality of source system changed from 1:1 to 1:n
 - **Allogeneic Transplantation**: Replace “CHOP or SNOMED CT“ in Standard column by “CHOP; SNOMED CT“, since it generated errors in SPHN tools
 - **Oncology Disease Assessment, Biochemical Disease Assessment, Clinical Disease Assessment, Radiological Disease Assessment**: Relaxed the constraint on code attribute from SNOMED CT (descendant of Staging and scales, desc. of Observable entity, desc. of Procedure), as it is the case for the parent Assessment
 - **Molecular Test**: relaxed the constraint on 'biomarker feature code' attribute to NCIT (desc. of Biomarker, desc. of Genomic Instability, desc. of  Mismatch Repair Deficiency) and SNOMED CT (desc. of Biochemical tumor marker)
 - **Antibody, Marker Panel, Antitumor Drug Screening Assay, Drug Panel, Antitumor Drug Screening Condition, Imaging Mass Cytometry Assay, Research Instrument, Pathology Image Digitization**: new concepts for omics data




## External terminologies

### Changes from SPO Schema 2024.1

 - OncoTree is now provided by SPHN
 - ICD-O-3 is now provided by SPHN

The external terminologies provided by SPHN can be found in [SPHN Ontology Service](https://terminology.dcc.sib.swiss/)
The project-specific external terminologies used by SPO are **NCI Thesaurus**.
[link to the external terminology repo](https://git.dcc.sib.swiss/project-space/spo/external-terminologies/-/releases/spo-schema-2025.1).



## Git clone

The first time you clone the directory, run the following command to also pull the SPHN 2023.2 in sphn-ontology folder:
```shell
git submodule update --init
``` 