# RDF Schema

Use this repo for RDF schema, dataset and documentation.

> ⚠️ In order to be more consistent with the dataset implementation of other projects, the nomenclature of the versions have changed. The previously called **Version 1**, that only contains a reference to SPHN dataset and no SPO concept, is now **Initial test**. The current **Version 1** is the actual first release of the SPO dataset. ⚠️

Find more details in:
- [Initial test](initial-test)
- [Version 1](version-1) this version (SPO Schema 2024.1) contains new concepts that extend SPHN Schema 2024.2
- [**Version 2**](version-2) this version (SPO Schema 2025.1)contains new concepts that extend SPHN Schema 2025.1
